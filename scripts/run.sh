#!/bin/sh

appName="null"
if [ -z "$1" ];then
    echo "must have appName paramater example: ./run.sh appName apolloOtp"
    exit
else
    appName=$1
fi
apolloOpt=$2
springOpt=$3
echo "running app $appName ---------"

#####################################################################################################################
# COMMAND LINE VARIABLES
#enviroment FIRST ARGUMENT 
# Ex: dev | sit | uat
env=default
# deploy port SECOND ARGUMENT
# THIRD ARGUMENT project name, deploy folder name and jar name
projectName=$appName
# FOURTH ARGUMENT external config file name
# Ex: application-localhost.yml
configFile=""

basePath=/home/tomcat
#### CONFIGURABLE VARIABLES ######
#destination absolute path. It must be pre created or you can
# improve this script to create if not exists
backupPath=backup
#configFolder=resources
##############################################################

#####
##### DONT CHANGE HERE ##############
#jar file
destFile=$projectName.jar


#CONSTANTS
logFile=out.log
dstLogFilePath=/data/logs/$projectName
dstLogFile=$dstLogFilePath/$logFile
#whatToFind="Started Application in"
whatToFind="Started "
msgLogFileCreated="$logFile created"
msgBuffer="Buffering: "
msgAppStarted="Application Started... exiting buffer!"

backupFile=""
### FUNCTIONS
##############
function stopServer(){
  if [ -n "$projectName" ]
  then
    echo "Stoping process is $projectName"
  	pid=`ps -ef | grep $projectName.jar | grep -v grep | awk '{print $2}'`
  	if [ -n "$pid" ]
  	then
  	   kill -9 $pid
  	fi
      echo " "
  fi
}

JAVA_OPTS=""
JAVA_MEM_OPTS=""
JAVA_DEBUG_OPTS=""
JAVA_JMX_OPTS=""
function run(){
    JAVA_OPTS=" -Djava.awt.headless=true -Djava.net.preferIPv4Stack=true"
    JAVA_MEM_OPTS="-server -Xmx512M -Xms512M"
    APOLLO_OPT=$apolloOpt
    SPRING_OPT=$springOpt
    SPRING_LOG_OPT="--logging.file=/data/logs/$projectName/$projectName.log"

	# BITS=`java -version 2>&1 | grep -i 64-bit`
	# if [ -n "$BITS" ]; then
	# 	JAVA_MEM_OPTS=" -server -Xmx1g -Xms1g -Xmn512m -XX:PermSize=512m -Xss256k -XX:+DisableExplicitGC -XX:+UseConcMarkSweepGC -XX:+CMSParallelRemarkEnabled -XX:+UseCMSCompactAtFullCollection -XX:LargePageSizeInBytes=128m -XX:+UseFastAccessorMethods -XX:+UseCMSInitiatingOccupancyOnly -XX:CMSInitiatingOccupancyFraction=70 "
	# else
	# 	JAVA_MEM_OPTS=" -server -Xms1g -Xmx2g -XX:PermSize=1024m -XX:SurvivorRatio=2 -XX:+UseParallelGC "
	# fi
	# JAVA_DEBUG_OPTS=""
	# if [ "$enable_debug" = "true" ]; then
	# 	JAVA_DEBUG_OPTS=" -Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,address=5566,server=y,suspend=n "
	# fi
	# JAVA_JMX_OPTS=""
	# if [ "$enable_jmx" = "true" ]; then
	# 	JAVA_JMX_OPTS=" -Dcom.sun.management.jmxremote.port=1099 -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false "
	# fi
   #echo "java -jar $destFile $properties" | at now + 1 minutes
   mkdir -p $dstLogFilePath
   touch $dstLogFile 
   chmod 777 $dstLogFile
   nohup nice java $JAVA_OPTS $APOLLO_OPT $JAVA_MEM_OPTS $JAVA_DEBUG_OPTS $JAVA_JMX_OPTS -jar $destFile $SPRING_OPT $SPRING_LOG_OPT $> $dstLogFile 2>&1 &
   echo "COMMAND: nohup nice java $APOLLO_OPT $JAVA_MEM_OPTS -jar $destFile $SPRING_OPT $SPRING_LOG_OPT $> $dstLogFile 2>&1 &"
   echo " "
}
function changeFilePermission(){
    echo "Changing File Permission: chmod 777 $destFile"
    chmod 777 $destFile
    echo " "
}   

function watch(){
    tail -f $dstLogFile |
        while IFS= read line
        do
            echo "$msgBuffer" "$line"

            if [[ "$line" == *"$whatToFind"* ]]; then
                echo $msgAppStarted
                pkill  tail
            fi
        done
}

### FUNCTIONS CALLS
#####################
# Use Example of this file. Args: enviroment | port | project name | external resourcce
# BUILD_ID=dontKillMe /path/to/this/file/api-deploy.sh dev 8082 spring-boot application-localhost.yml

# 1 - stop server on port ...
stopServer
#backup
# 2 - delete destinations folder content
#deleteFiles

# 3 - copy files to deploy dir
#copyFiles

changeFilePermission
# 4 - start server
run

# 5 - watch loading messages until  ($whatToFind) message is found
watch
